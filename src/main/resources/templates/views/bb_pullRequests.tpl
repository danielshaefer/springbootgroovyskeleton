import java.time.Instant;
import java.time.ZoneOffset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.ma.model.UTC;

layout 'layouts/main.tpl',
        pageTitle: 'Bitbucket Pull Request Info',
        mainBody: contents {
            raw.values.each { pr ->
                div(style:"border:1px solid black") {
                    div {
                        span("Project: $pr.toRef.repository.project.key, Repo: $pr.toRef.repository.name ")
                        a(href:"$host/$pr.link.url", "Pull Request Id: $pr.id")
                    }
                    div {
                        yield "Author: $pr.author.user.displayName"
                    }
                    div {
                        pr.reviewers.addAll(pr.participants);
                        def approvers = pr.reviewers.collect {
                            if (it.approved)
                                return "$it.user.displayName"
                        }
                        yield "Approvers: " + approvers.findAll().join(",")
                    }
                    div {

                        yield "Updated Date: " + UTC.fromMillis(pr.updatedDate.toLong()) + " UTC"
                    }
                }
            }
        }