package org.ma.model

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

/**
 * Created by dshaefer on 1/29/2016.
 */
class UTC {

    public static String fromMillis(long millisSinceEpoch) {
        def dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss")
        LocalDateTime.ofInstant(Instant.ofEpochMilli(millisSinceEpoch), ZoneOffset.UTC).format(dtf)
    }

}
