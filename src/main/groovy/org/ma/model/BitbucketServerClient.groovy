package org.ma.model

import groovy.json.JsonSlurper

/**
 * Created by dshaefer on 1/29/2016.
 */
class BitbucketServerClient {

    String host;
    String pathToCredentials

    public BitbucketServerClient(host) {
        this.host = host;
    }

    public BitbucketServerClient withCredentials(pathToCredentials) {
        this.pathToCredentials = pathToCredentials
        return this
    }

    public def getBitbucketUser(username) {
        def addr = buildBitbucketUserRequestUrl(host, username)
        println addr
        def conn = new UrlBuilder(addr).toConnection()
        if( conn.responseCode == 200 ) {
            new JsonSlurper().parseText( conn.content.text )
        } else {
            println "Something bad happened."
            println "${conn.responseCode}: ${conn.responseMessage}"
            throw new RuntimeException(conn.responseMessage)
        }
    }

    public def getPullRequestsForProject(project, repo) {
        def addr = buildMergedPullRequestUrl(host, project, repo);
        def url = new UrlBuilder(addr)
        if (pathToCredentials)
            url.withAuth(pathToCredentials);
        def conn = url.toConnection();
        if( conn.responseCode == 200 ) {
            def data = new JsonSlurper().parseText( conn.content.text )
            println "Response $data"
            def links = data.values.link.url;
            return [results: links,
                    host: host,
                    raw: data];
        } else {
            println "Something bad happened."
            println "${conn.responseCode}: ${conn.responseMessage}"
            throw new RuntimeException($conn.responseMessage)
        }
    }

    private String buildBitbucketUserRequestUrl(host, username) {
        "$host/api/2.0/users/$username"
    }

    private String buildMergedPullRequestUrl(host, project, repo) {
        "$host/rest/api/1.0/projects/$project/repos/$repo/pull-requests?state=merged";
    }

}
