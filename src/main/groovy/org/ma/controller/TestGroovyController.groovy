package org.ma.controller

import groovy.json.JsonSlurper
import org.ma.model.BitbucketServerClient
import org.ma.model.UrlBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.Banner
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

import java.nio.file.Path
import java.time.format.DateTimeFormatter

/**
 * Groovy Annotated Rest Controller*
 */
@RestController
@RequestMapping("/")
class TestGroovyController {

    private RequestMappingHandlerMapping requestMapping

    @Autowired
    public TestGroovyController(RequestMappingHandlerMapping requestMapping) {
        this.requestMapping = requestMapping
    }
    
    @RequestMapping("/")
    String index() {
        def endpoints = requestMapping.getHandlerMethods().collect{it.key.patternsCondition.patterns}.join(",<br>")
        "Mapped Endpoints: $endpoints"
        
    }

    /**
     * Example endpoint returning using Groovy Template files*
     * @return
     */
    @RequestMapping("/home") //this mapping is currently necessary to handle trailing slashes throwing errors.
    def home() {
        println "Home endpoint on Groovy Controller"
        new ModelAndView(
                    "views/home",
                    [bootVersion: Banner.package.implementationVersion,
                     groovyVersion: GroovySystem.version])
        
    }

    /**
     * Example of simple http calling in Groovy*
     */
    @RequestMapping("/bitbucket/user/{username}")
    def bitbucket(@PathVariable String username) {
        def client1 = new BitbucketServerClient("https://stash.dwolla.net").withCredentials("C:\\Users\\dshaefer\\.stash");
        def data = client1.getPullRequestsForProject("DWOL", "business-day-holiday-calculator");
        def client2 = new BitbucketServerClient("https://bitbucket.org")
        def userData = client2.getBitbucketUser(username)

        return new ModelAndView(
                "views/bb_user",
                userData)
    }

    /**
     * Example of simple http calling in Groovy*
     */
    @RequestMapping("/bitbucket/pullRequests/{project}/{repo}")
    def bitbucket(@PathVariable String project, @PathVariable String repo, @RequestParam(required = false) String raw) {
        def client1 = new BitbucketServerClient("https://stash.dwolla.net").withCredentials("C:\\Users\\dshaefer\\.stash");
        def data = client1.getPullRequestsForProject(project, repo);
        if (raw)
            return data.raw
        return new ModelAndView(
                "views/bb_pullRequests",
                data)
    }
    
}
