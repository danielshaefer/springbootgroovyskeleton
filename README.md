# SpringBootGroovySkeleton README #

This is a skeleton library from which we build a good Yeoman template for Groovy SpringBoot projects built with Gradle.

## SETUP ##

* Clone the repo
* Install Jdk 8
* Import into Intellij using Gradle and Auto-import (Verified works on Intellij 13-15)
* Check project settings are set to Jdk8
* Check that project has refreshed the Gradle dependencies (Gradle Tools Window -> can be found with Ctrl + Shift + a) Little blue refresh icon near top left.

### Resources ###

* [Spring Boot Getting Started Guide](https://spring.io/guides/gs/spring-boot/)
* [Groovy Template Engine + Spring Boot](https://spring.io/blog/2014/05/28/using-the-innovative-groovy-template-engine-in-spring-boot)